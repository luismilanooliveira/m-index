/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.Bucket;
import messif.buckets.BucketStorageException;
import messif.buckets.index.IndexComparator;
import messif.buckets.index.Search;
import messif.buckets.index.impl.AbstractSearch;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;
import messif.statistics.OperationStatistics;
import mindex.MIndexKey;
import mindex.MIndexKeyInterval;
import mindex.MetricIndexes;
import mindex.navigation.MIndexInternalCell;
import mindex.navigation.VoronoiLeafCell;
import mindex.navigation.MIndexLeafCell;

/**
 * Temporary memory storage of read objects (aggregated by pivot permutation) which
 *  is used during query evaluation especially when multi-buckets are used and
 *  some data may be already read.
 * The cache is thread-safe.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
*/
public class MultiBucketCache {
    
    /** Cache storage */
    protected Map<VoronoiLeafCell, AbstractObjectList<LocalAbstractObject>> cache = new HashMap<>();

    /** Visited buckets list */
    protected Set<Integer> visitedBuckets = new HashSet<>();
    
    private void putCluster(VoronoiLeafCell leafCell, AbstractObjectList<LocalAbstractObject> bucket) throws AlgorithmMethodException {
        if (cache.containsKey(leafCell)) {
            throw new AlgorithmMethodException("this cluster is already in the cache: " + leafCell);
        }
        cache.put(leafCell, bucket);
    }    
    
    public synchronized AbstractObjectList<LocalAbstractObject> processBucket(Bucket bucket, MIndexLeafCell leafCell) throws AlgorithmMethodException {
        AbstractObjectList<LocalAbstractObject> retVal;
        if (visitedBuckets.contains(bucket.getBucketID())) {
            retVal = cache.remove(leafCell);
            if (retVal == null) {
                throw new AlgorithmMethodException("leaf cell '" + leafCell.toString() + "' was empty but is to be processed locally");
            }
            return retVal;
        }
        
        visitedBuckets.add(bucket.getBucketID());
        short leafCellLevel = leafCell.getLevel();
        OperationStatistics.getOpStatisticCounter("AccessedBuckets").add();
        // find out, if this bucket is really a multibucket
        Map<Short,MIndexLeafCell> bucketNodes = ((MIndexInternalCell) leafCell.getParentNode()).getAllBucketNodes(bucket);
        if (bucketNodes.size() == 1) {
            retVal = new AbstractObjectList<>(bucket.getAllObjects());
        } else {
            for (VoronoiLeafCell structureLeafNode : bucketNodes.values()) {
                if (structureLeafNode.getObjectCount() > 0) {
                    putCluster(structureLeafNode, new AbstractObjectList<LocalAbstractObject>());
                }
            }
            for (AbstractObjectIterator<LocalAbstractObject> it = bucket.getAllObjects(); it.hasNext(); ) {
                LocalAbstractObject obj = it.next();
                cache.get(bucketNodes.get((MetricIndexes.readMIndexPP(obj).getPivotIndex(leafCellLevel)))).add(obj);
                //cache.get(MetricIndexes.readMIndexPP(obj).getClusterNumber(cellID.getLevel())).add(obj);
            }
            retVal  = cache.remove(leafCell);
            if (retVal == null) {
                throw new AlgorithmMethodException("cluster with key '" + leafCell.toString() + "' is empty but is to be processed locally");
            }
        }
        return retVal;
    }
    
    public Search<LocalAbstractObject> processBucketWithInterval(Bucket bucket, MIndexLeafCell cellID, IndexComparator<? super MIndexKey, ? super LocalAbstractObject> comparator, MIndexKeyInterval interval) throws AlgorithmMethodException {
        return new ObjectListSearch(processBucket(bucket, cellID), comparator, interval.getFrom(), interval.getTo());
    }
    
    public int getCurrentObjectCount() {
        int retVal = 0;
        for (AbstractObjectList<LocalAbstractObject> cluster : cache.values()) {
            retVal += cluster.size();
        }
        return retVal;
    }

    public int getCurrentCellCount() {
        return cache.size();
    }
    
    
    protected static class ObjectListSearch extends AbstractSearch<MIndexKey, LocalAbstractObject> {

        protected final AbstractObjectList<LocalAbstractObject> objects;

        protected int currentIndex = -1;
        
        public ObjectListSearch(AbstractObjectList<LocalAbstractObject> objects, IndexComparator<? super MIndexKey, ? super LocalAbstractObject> comparator, MIndexKey fromKey, MIndexKey toKey) {
            super(comparator, fromKey, toKey);
            this.objects = objects;
        }
        
        @Override
        protected LocalAbstractObject readNext() throws BucketStorageException {
            if (currentIndex < -1 || currentIndex >= objects.size() - 1) {
                return null;
            }
            return objects.get( ++ currentIndex );
        }

        @Override
        protected LocalAbstractObject readPrevious() throws BucketStorageException {
            if (currentIndex < 1 || currentIndex >= objects.size() + 1) {
                return null;
            }
            return objects.get( -- currentIndex );
        }

        @Override
        public void close() {
        }
        
    }
    
}

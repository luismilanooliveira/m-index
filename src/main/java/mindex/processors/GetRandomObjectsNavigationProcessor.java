/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mindex.processors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.AsynchronousNavigationProcessor;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.OperationErrorCode;
import messif.operations.query.GetRandomObjectsQueryOperation;
import mindex.navigation.VoronoiCell;
import mindex.navigation.VoronoiInternalCell;
import mindex.navigation.VoronoiLeafCell;

/**
 * Processor for the {@link GetRandomObjectsQueryOperation}. For each randomly selected object, it traverses
 *  the PPP-Tree in a random way and from the selected leaf, it returns a random object.
 * @author xnovak8
 */
public class GetRandomObjectsNavigationProcessor implements AsynchronousNavigationProcessor<GetRandomObjectsQueryOperation> {

    /** Operation to process. */
    private final GetRandomObjectsQueryOperation operation;
    
    /** Root of the PPP-Tree. */
    private final VoronoiInternalCell<? extends LocalAbstractObject> voronoiTreeRoot;

    protected final Random random = new Random(System.currentTimeMillis());

    /** Counter of the threads - each is selecting one object. */
    private final AtomicInteger counter;
    
    /**
     * Creates new process given an operation and PPP-Tree root. 
     * @param operation operation to process (defines number of objects to be returned)
     * @param voronoiTreeRoot root of the PPP-Tree
     */
    public GetRandomObjectsNavigationProcessor(GetRandomObjectsQueryOperation operation, VoronoiInternalCell<? extends LocalAbstractObject> voronoiTreeRoot) {
        this.operation = operation;
        this.voronoiTreeRoot = voronoiTreeRoot;
        this.counter = new AtomicInteger(operation.getCount());
    }    
    
    /**
     * This methods selects an object randomly from given leaf node.
     * @param leaf PPP-Tree leaf node to select an object from
     * @return randomly selected object
     */
    protected LocalAbstractObject getRandomObjectLeaf(VoronoiLeafCell leaf) {
        leaf.readLock();
        try {
            int leafCount = leaf.getObjectCount();
            if (leafCount <= 0) {
                return null;
            }
            int skip = random.nextInt(leafCount);
            int skipCounter = 0;
            AbstractObjectIterator<LocalAbstractObject> allObjects = leaf.getAllObjects();
            LocalAbstractObject retVal;
            do {
                retVal = allObjects.next();
            } while (allObjects.hasNext() && skipCounter++ < skip);
            return retVal;
        } finally {
            leaf.readUnLock();
        }
    }
    
    /**
     * This method selects an object from the given internal node (it calls the method recursively).
     * @param internal internal node of the PPP-Tree
     * @return selected object (from a leaf node)
     */
    protected LocalAbstractObject getRandomObjectInt(VoronoiInternalCell internal) {
        Iterator<Map.Entry<Short, VoronoiCell>> childIt = internal.getChildNodes();
        List<VoronoiCell<LocalAbstractObject>> childNodes = new ArrayList<>();
        while (childIt.hasNext()) {
            childNodes.add(childIt.next().getValue());
        }
        if (childNodes.isEmpty()) {
            return null;
        }
        
        LocalAbstractObject retVal = null;
        do {
            VoronoiCell<LocalAbstractObject> nextNode = childNodes.get(random.nextInt(childNodes.size()));
            if (nextNode instanceof VoronoiLeafCell) {
                retVal = getRandomObjectLeaf((VoronoiLeafCell) nextNode);
            }
            if (nextNode instanceof VoronoiInternalCell) {
                retVal = getRandomObjectInt((VoronoiInternalCell) nextNode);
            }
            childNodes.remove(nextNode);
        } while (retVal == null && (! childNodes.isEmpty()));
        
        return retVal;
    }
    
    @Override
    public Callable<GetRandomObjectsQueryOperation> processStepAsynchronously() throws InterruptedException {
        if (counter.getAndDecrement() <= 0) {
            return null;
        }
        return new Callable<GetRandomObjectsQueryOperation>() {
            @Override
            public GetRandomObjectsQueryOperation call() throws Exception {
                LocalAbstractObject nextRandom = getRandomObjectInt(voronoiTreeRoot);
                if (nextRandom != null) {
                    addToAnswer(nextRandom);
                }
                return operation;
            }
        };
    }

    @Override
    public boolean processStep() throws InterruptedException, AlgorithmMethodException, CloneNotSupportedException {
        if (counter.getAndDecrement() <= 0) {
            return false;
        }
        LocalAbstractObject nextRandom = getRandomObjectInt(voronoiTreeRoot);
        if (nextRandom != null) {
            addToAnswer(nextRandom);
        }
        return true;
    }

    /**
     * Simply add a next randomly selected object to the query answer. This method can be override.
     * @param nextRandom object to be inserted to the query answer
     */
    protected void addToAnswer(AbstractObject nextRandom) throws IllegalArgumentException {
        operation.addToAnswer(nextRandom);
    }    

    @Override
    public GetRandomObjectsQueryOperation getOperation() {
        return operation;
    }
    
    @Override
    public void close() {
        if (operation.getAnswerCount() >= operation.getCount()) {
            operation.endOperation();
        } else {
            operation.endOperation(OperationErrorCode.HAS_NEXT);
        }
    }

    @Override
    public boolean isFinished() {
        return counter.get() <= 0;
    }

    @Override
    public int getProcessedCount() {
        return operation.getCount() - counter.get();
    }

    @Override
    public int getRemainingCount() {
        return counter.get();
    }
    
}

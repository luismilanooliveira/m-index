/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.ObjectProvider;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.ObjectProvidersIterator;
import mindex.MetricIndex;
import mindex.MetricIndexes;

/**
 * Encapsulates information about a super-cluster in the M-Index dynamic cluster hierarchy.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 * @param <O> type of the data objects stored in this Voronoi node
 */
public abstract class VoronoiInternalCell<O extends LocalAbstractObject> extends VoronoiCell<O> implements NextLevelPivotChecker {

    /** Class id for serialization */
    private static final long serialVersionUID = 410201L;

    /** Max number of threads reading from this subtree simultaneously. */
    private static final int THREADS_MAX_NUMBER = 1000;
    
    /** Read/Write lock to make operation processing thread save. */
    private transient Semaphore lock = new Semaphore(THREADS_MAX_NUMBER, true);
    
    /** The pivot permutation prefix of this internal Voronoi cell; the length is equal to the node level */
    protected final short [] ppp;
    
    /** Child nodes - map of subclusters indexed according to pivot numbers in the next level. */
    protected final VoronoiCell<O> [] children;
    
    /**
     * Create new object initializing cluster number, level and parentNode. The created node is empty - no child nodes are created!
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * @param pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     * @throws AlgorithmMethodException if the cluster storage was not created successfully
     */
    public VoronoiInternalCell(MetricIndex mIndex, VoronoiInternalCell parentNode, short[] pivotCombination) throws AlgorithmMethodException {
        super(mIndex, parentNode);
        this.ppp = pivotCombination;
        this.children = new VoronoiCell[mIndex.getNumberOfPivots()];
    }

    /**
     * Create new lock after deserialization.
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        lock = new Semaphore(THREADS_MAX_NUMBER, true);
    }

    /**
     * Return the level M-Index of this cluster or super-cluster - the virtual tree root has level=0
     * @return the level M-Index of this cluster or super-cluster - the virtual tree root has level=0
     */
    @Override
    public short getLevel() {
        return (short) ppp.length;
    }
    
    @Override
    public short[] getPppForReading() {
        return ppp;
    }        

    @Override
    public short[] getPppForReading(short thisIndex) {
        return ppp;
    }    
    
    // ************************   Sub-level manipulation  *********************************** //
    
    /**
     * Return the sub-cluster given its pivot number at level {@link #level + 1}
     * @param pivotNumber pivot number at level {@link #level}
     * @return sub-cluster given its pivot number at level {@link #level + 1}
     */
    public final VoronoiCell getChildNode(int pivotNumber) {
        return children[pivotNumber];
    }

    @Override
    public boolean containsGivenPivot(int pivotNumber) {
        return children[pivotNumber] != null;
    }        

    /**
     * Return the PPP of the child node of this Voronoi cell.
     * @param node node of this voronoi cell
     * @return newly created array (of length "level + 1") with PPP of the passed child or NULL, if the node is not its child
     */
    public short [] getChildPPP(VoronoiCell node) {
        for (int i = 0; i < children.length; i++) {
            if (children[i] == node) {
                short [] retVal = Arrays.copyOf(ppp, ppp.length + 1);
                retVal[ppp.length] = (short) i;
                return retVal;
            }
        }
        MetricIndexes.logger.warning("Trying to getChildPPP for a child node that is not (no longer) child of this internal cell: " + ppp.toString());
        return null;
    }
    
    /**
     * Return the iterator over all (non-null) sub-clusters. 
     * @return iterator over all (non-null) sub-clusters. 
     */
    public Iterator<Map.Entry<Short,VoronoiCell<O>>> getChildNodes() {
        return new Iterator<Map.Entry<Short, VoronoiCell<O>>>() {
            
            private VoronoiCell nextCell = null;
            private int nextCellIndex = -1;
            
            @Override
            public boolean hasNext() {
                if (nextCell != null) {
                    return true;
                }
                do {
                    nextCellIndex ++;
                } while (nextCellIndex < children.length && (nextCell = children[nextCellIndex]) == null);
                
                return (nextCellIndex < children.length);
            }

            @Override
            public Map.Entry<Short, VoronoiCell<O>> next() {
                try {
                    return new Map.Entry<Short, VoronoiCell<O>>() {
                        private final Short key = (short) nextCellIndex;
                        private final VoronoiCell<O> value = nextCell;

                        @Override
                        public Short getKey() {
                            return key;
                        }

                        @Override
                        public VoronoiCell<O> getValue() {
                            return value;
                        }

                        @Override
                        public VoronoiCell<O> setValue(VoronoiCell<O> value) {
                            throw new UnsupportedOperationException("This entry is immutable."); 
                        }
                    };
                } finally {
                    nextCell = null;
                }
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("This iteraror is immutable");
            }
        };        
    }

    /**
     * This is an internal method to create a child node for given pivot number and it is put into the list of child nodes.
     *  If the cluster cannot exist, it is not created and null is returned.
     * @param childPivotNumber number of the child 
     * @param internalNode used if we know that internal node should be created (rare)
     * @return new created child node
     * @throws AlgorithmMethodException if the cluster storage was not created successfully
     */
    public VoronoiCell createChildNode(short childPivotNumber, boolean internalNode) throws AlgorithmMethodException {
        // skip the ones that cannot exist in cluster tree
        for (short j= 0; j < ppp.length; j++) {
            if ((ppp[j] == childPivotNumber)) {
                return null;
            }
        }
        short [] newCombination = Arrays.copyOf(ppp, ppp.length + 1);
        newCombination[ppp.length] = childPivotNumber;

        VoronoiCell child = createSpecificChildCell(internalNode, newCombination);
        putChildNode(childPivotNumber, child);
        return child;
    }

    /**
     * Creates a specific child according to the type of this internal cell (data/nodata etc.)
     * @param internalNode if to crate an internal or leaf cell
     * @param newCombination the new PPP combination
     * @return newly created cell
     */
    public abstract VoronoiCell createSpecificChildCell(boolean internalNode, short[] newCombination) throws AlgorithmMethodException;
    
    /**
     * Set a subcluster for a given pivot (return a previously associated node, if any, or null).
     * @param pivot pivot at level <code>{@link #level} + 1</code>
     * @param node sub-cluster to be put under this super-cluster
     */
    public void putChildNode(Short pivot, VoronoiCell node) {
        mIndex.setDynamicTreeModified(true);
        children[pivot] = node;
    }
    
    
    // ******************************     Implementation of abstract methods      ******************** //
    
    
    @Override
    public AbstractObjectIterator<O> getAllObjects() {
        Collection<ObjectProvider<? extends O>> subIterators = new ArrayList<>();
        for (VoronoiCell<O> child : children) {
            if (child != null) {
                subIterators.add(child.getAllObjects());
            }
        }
        return new ObjectProvidersIterator<>(subIterators);
    }

    @Override
    public int getObjectCount() {
        int retVal = 0;
        for (VoronoiCell<O> child : children) {
            if (child != null) {
                retVal += child.getObjectCount();
            }
        }
        return retVal;
    }

    @Override
    public int getObjectCountByObjects() {
        int retVal = 0;
        for (VoronoiCell<O> child : children) {
            if (child != null) {
                retVal += child.getObjectCountByObjects();
            }
        }
        return retVal;
    }
    
    @Override
    public VoronoiLeafCell getResponsibleLeaf(short [] givenPPP, boolean createPath) throws AlgorithmMethodException {
        VoronoiCell node = children[givenPPP[ppp.length]];
        if (node == null) {
            if (! createPath) {
                return null;
            }
            node = createChildNode(givenPPP[ppp.length], mIndex.getMinLevel() > ppp.length + 1);
        }
        return node.getResponsibleLeaf(givenPPP, createPath);
    }
    
    @Override
    public void clearData() throws AlgorithmMethodException {
        for (VoronoiCell<O> child : children) {
            if (child != null) {
                child.clearData();
            }
        }
    }
        
    @Override
    public String toString() {
        StringBuilder strBuf = new StringBuilder();
        printBasicInfo(strBuf, ppp);
        strBuf.append(" (supercluster)\n");
        for (int i = 0; i < children.length; i++) {
            if (children[i] != null) {
                if (children[i] instanceof VoronoiLeafCell) {
                    printBasicInfo(strBuf, ppp, (short) i);
                }
                strBuf.append(children[i].toString());
            }
        }
        return strBuf.toString();
    }

    @Override
    public void calculateTreeSize(Map<Short,AtomicInteger> intCellNumbers, Map<Short, AtomicLong> branchingSums, AtomicInteger leafCellNumber, AtomicLong dataSizeBytes, AtomicLong leafLevelSum) {
        int numberOfChildren = 0;
        for (VoronoiCell<O> child : children) {
            if (child != null) {
                child.calculateTreeSize(intCellNumbers, branchingSums, leafCellNumber, dataSizeBytes, leafLevelSum);
                numberOfChildren ++;
            }
        }
        intCellNumbers.get(getLevel()).incrementAndGet();
        branchingSums.get(getLevel()).addAndGet(numberOfChildren);        
    }
    
    
    // *******************************   Auxiliary  methods   **************************** //
    
    @Override
    public void readLock() {
        lock.acquireUninterruptibly();
    }
    
    @Override
    public void readUnLock() {
        lock.release();
    }
    
    @Override
    public void writeLock() {
        lock.acquireUninterruptibly(THREADS_MAX_NUMBER);
    }
    
    @Override
    public void writeUnLock() {
        lock.release(THREADS_MAX_NUMBER);
    }
    
}

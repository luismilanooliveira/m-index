/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.Bucket;
import messif.buckets.BucketErrorCode;
import messif.buckets.BucketStorageException;
import messif.buckets.CapacityFullException;
import messif.buckets.LocalBucket;
import messif.buckets.split.SplitResult;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.QueryOperation;
import messif.statistics.StatisticCounter;
import mindex.MetricIndex;
import mindex.MetricIndexes;

/**
 * Abstract class encapsulates a leaf node of a cluster tree for a dynamic M-Index.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexLeafCell extends VoronoiLeafCell<LocalAbstractObject> {

    /** Class id for serialization. */
    private static final long serialVersionUID = 41204L;
    
    /** Statistic counter of number of objects written to disk during building phase (inserts and reorganizations = splits). */
    public static StatisticCounter buildingIOCosts = StatisticCounter.getStatistics("BuildIOCosts");
    
    /** Bucket storage - potentially null, if no objects stored in this node yet. */
    private Bucket storage;

    /**
     * Constructor given all parameters mandatory for the parent class. The storage and the covered interval are set to null. 
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * @param pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     */
    public MIndexLeafCell(MetricIndex mIndex, VoronoiInternalCell parentNode, short[] pivotCombination) {
        super(mIndex, parentNode);
    }
        
    /**
     * Read the serialized disk storage from an object stream.
     * @param in the object stream from which to read the disk storage
     * @throws IOException if there was an I/O error during deserialization
     * @throws ClassNotFoundException if there was an unknown object in the stream
     */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        // Proceed with standard deserialization first
        in.defaultReadObject();
        int i = 0;
    }
    
    // ***********************      Getters and Setters   ********************************* //
    
    public Bucket getStorage() {
        return storage;
    }
    
    /**
     * Return iterator over all objects in the storage - the storage mustn't be null.
     * @return iterator over all objects in the storage
     */
    @Override
    public AbstractObjectIterator<LocalAbstractObject> getAllObjects() {
        return storage.getAllObjects();
    }
    

    // ***************************      Data and storage manipulation (implementation)    ********************* //
    
    /**
     * Tries to insert all the objects into the bucket managed by this LeafCell.
     * If {@code CapacityFullException} is thrown during insertion a list of
     * objects that were not inserted if returned
     * @param objects to be inserted to the bucket
     * @return null of the configuration for the split to be realized, if the node is full; the configuration
     *  contains the the list of not inserted objects
     * @throws AlgorithmMethodException 
     */
    @Override
    public SplitConfiguration insertObjects(List<LocalAbstractObject> objects) throws AlgorithmMethodException {
        try {
            Bucket bucket = getOrCreateStorage();
            bucket.addObjects(objects);
            //objectCount.addAndGet(objects.size());
            objectCount += (objects.size());
        } catch (CapacityFullException e) {
            // update counter and interval for the objects that were really inserted before the capacity limit was reached
            //objectCount.addAndGet(e.getNumberOfInsertedObjects());
            objectCount += e.getNumberOfInsertedObjects();
            return new SplitConfiguration(false, e.getNumberOfInsertedObjects(), objects.subList(e.getNumberOfInsertedObjects(), objects.size()), storage);
        } catch (BucketStorageException e) {
            throw new AlgorithmMethodException("Unable to insert some objects into the bucket!", e);
        }
        if (checkAgileMultibucketPartitioning()) {
            return new SplitConfiguration(true, objects.size(), Collections.EMPTY_LIST, storage);
        }
        return null;
    }
    
    /**
     * Method that deletes given object from corresponding Bucket. TODO: return and apply "merge configuration".
     * @param objects to be deleted
     * @param deleteLimit maximum number of objects that match given object to be deleted
     * @param checkLocator if true, then the object is deleted according to object locator
     * @return number of objects actually deleted by this method
     * @throws AlgorithmMethodException 
     */
    @Override
    public Collection<LocalAbstractObject> deleteObjects(List<LocalAbstractObject> objects, int deleteLimit, boolean checkLocator) throws AlgorithmMethodException {
        try {
            if (storage == null) {
                return objects;
            }
            int overallDeleted = 0;
            Collection<LocalAbstractObject> notDeleted = new ArrayList();
            for (LocalAbstractObject object : objects) {            
                int deleted = (checkLocator && object.getLocatorURI() != null) ?
                        storage.deleteObject(object.getLocatorURI(), deleteLimit) :
                        storage.deleteObject(object, deleteLimit);
                if (deleted <= 0) {
                    notDeleted.add(object);
                }
                overallDeleted += deleted;
            }
            objectCount -= overallDeleted;
            return notDeleted;
        } catch (BucketStorageException e)  {
            if (e.getErrorCode().equals(BucketErrorCode.LOWOCCUPATION_EXCEEDED)) {
                throw new AlgorithmMethodException("Unable to delete an object because LowOccupation is set > 0", e);
            }
            throw new AlgorithmMethodException("Unable to delete an object!", e);
        }
    }
    
    /**
     * This method should be called only when the {@code insertObjects} method returns non-empty list of objects
     * (= {@code CapacityFullException} was throw during insertion of objects into bucket managed by this LeafCell)
     * or when the {@code checkAgileMultibucketPartitioning} returns true
     * @param splitConfiguration
     * @return
     * @throws AlgorithmMethodException 
     */
    @Override
    public boolean split(SplitConfiguration splitConfiguration) throws AlgorithmMethodException {        
        if (! isValid() || storage != splitConfiguration.getBucketToBeSplit()) {
            return false;
        }
        
        // first, try to split only the multi-bucket into the nodes that share this bucket
        if ((storage instanceof LocalBucket) && mIndex.isMultiBuckets()) {
            MultiBucket multiBucket = new MultiBucket((LocalBucket) storage, parentNode);
            if (multiBucket.prepareSplit(! splitConfiguration.isAgileMultibucketPartitioning())) {
                partitionMultiBucket(multiBucket);
                mIndex.setDynamicTreeModified(true);
                mIndex.removeBucket(multiBucket.getBucket(), false);
                return true;
            }
            if (splitConfiguration.isAgileMultibucketPartitioning()) {
                MetricIndexes.logger.log(Level.WARNING, "The agile split was initiated but then could not be done: level={0}, ", getLevel());
                return false;
            }
        }
        
        // otherwise do normal split unless we have reached the maximal level
        if (getLevel() < mIndex.getMaxLevel()) {
            invalidateThisNode(split());
            return true;
        } 

        // if the bucket cannot be split anyhow, build internal M-Index on it (if defined)
        storage = mIndex.reorganizeOvefilled(storage);
        return true;        
    }

    @Override
    public int processOperation(QueryOperation operation) throws AlgorithmMethodException {
        return storage.processQuery(operation);
    }
    
    @Override
    public void calculateTreeSize(Map<Short,AtomicInteger> intCellNumbers, Map<Short, AtomicLong> branchingSums, AtomicInteger leafCellNumber, AtomicLong dataSizeBytes, AtomicLong leafLevelSum) {
        leafCellNumber.incrementAndGet();
        leafLevelSum.addAndGet(getLevel());
        // THIS does not work because the capacity does not have to be in bytes and because of multi-buckets
//        if (storage instanceof LocalBucket) {
//            dataSizeBytes.addAndGet(((LocalBucket) storage).getOccupation());
//        }
    }

    
    // *********************************    Private methods    *************************** //
        
    /**
     * Checks that the bucket is created and that it does not require reorganization
     * @return non-null bucket
     * @throws AlgorithmMethodException if the bucket creation fails
     */
    private Bucket getOrCreateStorage() throws AlgorithmMethodException {
        try {
            // if NetworkBucketDispatcher is being used then create RemoteBucket
            if (storage == null) {
                storage = mIndex.createBucket(this);
            }
            return storage;
        } catch (IOException | BucketStorageException e) {
            throw new AlgorithmMethodException("Failed to create a new bucket!", e);
        }
    }    
    
    /**
     * Checks the occupation of this leaf cell and its (multi-)bucket and calls split procedures, if necessary.
     * @throws AlgorithmMethodException 
     */
    private boolean checkAgileMultibucketPartitioning() {
        if (!mIndex.isAgile() || !(storage instanceof LocalBucket)) {
            return false;
        }
        
        // if this multi-bucket CAN be split (and the Agile approach is used)
        return new MultiBucket((LocalBucket) storage, parentNode).prepareSplit(false);
    }
    
    /**
     * Partition this leaf node one level further - create a new internal node and store the stored object to it.
     *
     * @return true, if the split was successful
     * @throws messif.algorithms.AlgorithmMethodException      
     */
    private VoronoiInternalCell<LocalAbstractObject> split() throws AlgorithmMethodException {
        short [] thisNodePPP = parentNode.getChildPPP(this);
        VoronoiInternalCell newNode = (VoronoiInternalCell) parentNode.createSpecificChildCell(true, thisNodePPP);
        
        Bucket originalBucket = storage;
        SplitPolicyMIndexPartitioning policy = new SplitPolicyMIndexPartitioning((short) (thisNodePPP.length + 1), mIndex.isPreciseSearch(), mIndex.getNumberOfPivots());
        boolean wasBucketSplit = mIndex.isMultiBuckets() ? splitCreateMultiBuckets(newNode, policy) : splitCreateSingleBuckets(newNode, policy);
        
        parentNode.putChildNode(thisNodePPP[thisNodePPP.length - 1], newNode);
        if (wasBucketSplit) {
            // remove the original bucket
            mIndex.removeBucket(originalBucket, false);            
        }
        return newNode;
    }

    /**
     * Partition this leaf node one level further - create a new internal node and store the stored object to it.
     * Each new node has its own new bucket.
     * @return true, if the split was successful
     * @throws messif.algorithms.AlgorithmMethodException 
     */
    private boolean splitCreateSingleBuckets(VoronoiInternalCell newNode, SplitPolicyMIndexPartitioning policy) throws AlgorithmMethodException {    
        try {
            SplitResult splitResut = mIndex.splitLeafBucket(storage, policy, -1);
            List<? extends Bucket> newBuckets = splitResut.getBuckets(mIndex.getBucketDispatcher());
            policy = (SplitPolicyMIndexPartitioning) splitResut.getSplitPolicy();
            
            for (short i = 0; i < newBuckets.size(); i++) {
                if (policy.getOccupation(i) <= 0) {
                    continue;
                }
                MIndexLeafCell newLeaf = (MIndexLeafCell) newNode.createChildNode(i, false);
                if (newLeaf != null && newBuckets.get(i) != null) {
                    newLeaf.setStorageAfterSplit(newBuckets.get(i), policy, i);
                }
            }        
            //buildingIOCosts.add(objectCount.get());
            buildingIOCosts.add(objectCount);
            return true;
        } catch (IllegalArgumentException | BucketStorageException ex) {
            throw new AlgorithmMethodException(ex);
        }
    }
        
    /**
     * Partition this leaf node one level further - create a new internal node and store the stored object to it.
     *  No new bucket is actually created by this method - all data stay in one multi-bucket
     * @return true, if the split was successful (at least 2 non-empty leaf nodes were created)
     * @throws messif.algorithms.AlgorithmMethodException      
     */
    private boolean splitCreateMultiBuckets(VoronoiInternalCell newNode, SplitPolicyMIndexPartitioning policy) throws AlgorithmMethodException {
        if (!(storage instanceof LocalBucket)) {
            throw new AlgorithmMethodException("Multi-buckets cannot be crated on non-local buckets");
        }
        AbstractObjectIterator<LocalAbstractObject> iterator = storage.getAllObjects();
        // iterate over all stored objects to find out individual cells occupations (and covered intervals)
        while (iterator.hasNext()) {
            policy.match(iterator.next());
        }
        
        // create new logical nodes and set their occupation
        for (short i = 0; i < mIndex.getNumberOfPivots(); i++) {
            MIndexLeafCell newLeaf = (MIndexLeafCell) newNode.createChildNode(i, false);
            if (newLeaf != null) {
                newLeaf.setStorageAfterSplit(storage, policy, i);
            } 
        }
        // partition the newly created multibucket (force at least one split)
        MultiBucket multiBucket = new MultiBucket((LocalBucket) storage, newNode);
        if (! multiBucket.prepareSplit(true)) {
            return false;
        }

        partitionMultiBucket(multiBucket);
        return true;
    }

    /**
     * Sets the given storage to this node after a split was realized using the given split policy;
     *  also the new object count must be set.
     * @param storage new data bucket
     * @param policy used split policy
     * @param partitionID id of the new partition from the split policy
     */
    protected void setStorageAfterSplit(Bucket storage, SplitPolicyMIndexPartitioning policy, int partitionID) {
        this.storage = storage;
        //this.objectCount.set(policy.getOccupation(partitionID));
        this.objectCount = policy.getOccupation(partitionID);
    }    
    
    
    /**
     * Partition this cells multi-bucket into single buckets
     * @throws messif.algorithms.AlgorithmMethodException if new bucket cannot be created
     */
    private void partitionMultiBucket(MultiBucket multiBucket) throws AlgorithmMethodException {
        if (multiBucket.getBucket() != storage) {
            throw new AlgorithmMethodException("trying to split multi-bucket which is not in this leaf node");
        }
        List<Bucket> newBuckets = new ArrayList<>(multiBucket.getPartitionsCount());
        try {
            storage.split(multiBucket, newBuckets, mIndex.getBucketDispatcher(), -1);
            for (MIndexLeafCell node : multiBucket.getNodes()) {
                node.storage = newBuckets.get(multiBucket.getNodeBucketAfterSplit(node));
            }
            buildingIOCosts.add(multiBucket.getBucket().getObjectCount());
        } catch (IllegalArgumentException | BucketStorageException ex) {
            throw new AlgorithmMethodException(ex);
        }            
    }    
    
    @Override
    public void clearData() throws AlgorithmMethodException {
        if (storage != null) {
            mIndex.removeBucket(storage, true);
            storage = null;
        }
    }

}

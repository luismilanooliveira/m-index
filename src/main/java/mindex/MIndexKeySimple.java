/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.IOException;
import java.io.Serializable;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializable;
import messif.objects.nio.BinarySerializator;

/**
 * The simple M-Index object key that contains a long (cluster), a float (distance) and a locator URI.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexKeySimple implements MIndexKey, Serializable, BinarySerializable {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 742101L;

    /** The integral part of the key - the cluster number */
    protected long cluster;
    
    /** The decimal part of the key - the normalized distance between and object and its pivot. */
    protected float normalizedDistance;

    @Override
    public long getClusterNumber() {
        return cluster;
    }
    
    @Override
    public float getNormalizedDistance() {
        return normalizedDistance;
    }
    
    
    /**
     * Creates new instance of the key having all necessary values.
     * @param locatorURI locator of the object (key)
     * @param cluster the integral part of the key - cluster number, must be non-negative!
     * @param normalizedDistance the decimal part of the key - the normalized distance between and object and its pivot [0,1)
     */
    public MIndexKeySimple(long cluster, float normalizedDistance) {
        if ((cluster < 0) || (normalizedDistance >= 1f) || (normalizedDistance < 0f)) {
            throw new IllegalArgumentException("The decimal part "+normalizedDistance+" should be normalized to [0,1) and cluster "+cluster+" should be non-negative");
        }
        this.cluster = cluster;
        this.normalizedDistance = normalizedDistance;
    }


   /**
     * Compare the keys according to the cluster.distance value
     * @param o the key to compare this key with
     */
    @Override
    public int compareTo(MIndexKey o) {
        if (! (o instanceof MIndexKey)) {
            return 3;
        }
        long diff = getClusterNumber() - o.getClusterNumber();
        
        if (diff == 0l) {
            return Float.compare(getNormalizedDistance(), o.getNormalizedDistance());
        }
        return (diff < 0l) ? -1 : 1;
    }


    
    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of MIndexMutableKey loaded from binary input stream.
     * 
     * @param input the stream to read the key from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the stream
     */
    protected MIndexKeySimple(BinaryInput input, BinarySerializator serializator) throws IOException {
        cluster = serializator.readLong(input);
        normalizedDistance = serializator.readFloat(input);
    }

    /**
     * Binary-serialize this object into the <code>output</code>.
     * @param output the output stream this object is binary-serialized into
     * @param serializator the serializator used to write objects
     * @return the number of bytes actually written
     * @throws IOException if there was an I/O error during serialization
     */
    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return serializator.write(output, cluster) + serializator.write(output, normalizedDistance);
    }

    /**
     * Returns the exact size of the binary-serialized version of this object in bytes.
     * @param serializator the serializator used to write objects
     * @return size of the binary-serialized version of this object
     */
    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return 8 + 4;
    }

    @Override
    public String toString() {
        return (new StringBuilder()).append(getClusterNumber()).append(Float.toString(getNormalizedDistance()).substring(1)).toString();
    }
    
}

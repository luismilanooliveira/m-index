/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.NavigationDirectory;
import messif.algorithms.NavigationProcessor;
import messif.buckets.Bucket;
import messif.buckets.BucketDispatcher;
import messif.buckets.BucketStorageException;
import messif.buckets.CapacityFullException;
import messif.buckets.LocalBucket;
import messif.buckets.split.SplitPolicy;
import messif.buckets.split.SplitResult;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.Approximate;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.DataManipulationOperation;
import messif.operations.data.DeleteOperation;
import messif.operations.data.InsertOperation;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.operations.query.ApproxRangeQueryOperation;
import messif.operations.query.GetRandomObjectsQueryOperation;
import messif.operations.query.KNNQueryOperation;
import messif.operations.query.RangeQueryOperation;
import messif.utility.ExtendedPropertiesException;
import mindex.navigation.MIndexInternalCell;
import mindex.navigation.MIndexLeafCell;
import mindex.navigation.MIndexPreciseInternalCell;
import mindex.navigation.VoronoiInternalCell;
import mindex.navigation.VoronoiLeafCell;
import mindex.processors.ApproxNavigationProcessor;
import mindex.processors.GetRandomObjectsNavigationProcessor;
import mindex.processors.KNNProgressiveNavigationProcessor;
import mindex.processors.RangeNavigationProcessor;
import mindex.processors.DataManipulationNavigationProcessor;

/**
 * This class carries the core functionality of the M-Index:
 * <ul>
 *  <li> having an object, it calculates its M-Index key</li>
 *  <li> having a range query, it determines the intervals to be searched</li>
 *  <li> having a cluster id, it determines its score (according to the approx. heuristic)</li>
 *  <li> having an approx. query, it determines the clusters to be searched (and parts within the clusters)</li>
 * </ul>
 * 
 * It also carries the parameters of the M-Index - maximum level, (minimum level), and pivots
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MetricIndex implements NavigationDirectory, java.io.Serializable, ThreadPoolUser {

    /** Class serial ID for serialization */
    private static final long serialVersionUID = 11211L;
    
    /** The whole M-Index configuration is kept in the extended properties file */
    protected final MIndexProperties configuration;
    
    /** The M-Index structure - dynamic levels */
    protected final VoronoiInternalCell voronoiCellTree;
    
    /** The bucket dispatcher for buckets in the clusterStructure */
    protected BucketDispatcher bucketDispatcher;

    /** Class for small clusters */
    private Class<? extends LocalBucket> overfilledBucketClass;

    /** Class for small clusters */
    private Map<String, Object> overfilledBucketParams;

    /** This field encapsulates the M-Index pivot and calculation of PP */
    protected final MIndexObjectInitilizer ppCalculator;
    
    /** Flag that says whether the dynamic cell tree was modified and should be serialized */
    protected transient volatile boolean dynamicTreeModified = false;
    
    //************************************   Getters ************************//

    /** Returns the M-Index Pivot Permutation (PP) calculator */
    public MIndexObjectInitilizer getPPCalculator() {
        return ppCalculator;
    }
    
    /** Returns this M-Index bucket dispatcher */
    public BucketDispatcher getBucketDispatcher() {
        return bucketDispatcher;
    }

    /** Returns the dynamic M-Index tree root */
    public VoronoiInternalCell getVoronoiCellTree() {
        return voronoiCellTree;
    }
    
    /** Number of pivots (it IS always known). */
    public short getNumberOfPivots() {
        return ppCalculator.getNumberOfPivots();
    }
    
    /** Return the maximal distance of given metric space (if the pivots are set). */
    public float getMaxDistance() {
        return ppCalculator.getMaxDistance();
    }

    /**
     * Returns distance between given two pivots, if the pivots are set; it is 
     *  precomputed in a pivot x pivot matrix.
     * @param pivot1 the first pivot index
     * @param pivot2 the second pivot index
     * @return distance between the pivots or -1 (if pivots not set)
     */
    public float getPivotsDistance(int pivot1, int pivot2) {
        return ppCalculator.getPivotsDistance(pivot1, pivot2);
    }
    
    /** Return the maximal level of the M-Index. */
    public short getMaxLevel() {
        return configuration.getShortProperty(MIndexProperty.MAX_LEVEL);
    }

    /** Return the minimal level of the M-Index. */
    public short getMinLevel() {
        return configuration.getShortProperty(MIndexProperty.MIN_LEVEL);
    }
    
    /** Returns the minimal occupation of a self-standing disk bucket; if > 0 then multi-buckets can be created. */
    public long getBucketMinOccupation() {
        return configuration.getLongProperty(MIndexProperty.BUCKET_MIN_OCCUPATION);
    }
    
    /** Return the class of this M-Index. */
    public final Class<? extends LocalAbstractObject> getObjectClass() {
        return ppCalculator.getObjectClass();
    }
    
    public boolean isSpecialOverfilledBuckets() {
        return configuration.getBoolProperty(MIndexProperty.BUCKET_OVERFILLED_SPECIAL);
    }
        
    public boolean isRemoveDuplicates() {
        return configuration.getBoolProperty(MIndexProperty.REMOVE_DUPLICATES);
    }
        
    public int getApproxDefaultParam() {
        return configuration.getIntProperty(MIndexProperty.APPROX_DEFAULT);
    }

    public Approximate.LocalSearchType getApproxDefaultType() {
        return Approximate.LocalSearchType.valueOf(configuration.getProperty(MIndexProperty.APPROX_DEFAULT_TYPE));
    }   

    public boolean isForceDefaultApprox() {
        return configuration.getBoolProperty(MIndexProperty.APPROX_FORCE_DEFAULT);
    }   

    public boolean isApproxProcessWholeMultiBucket() {
        return configuration.getBoolProperty(MIndexProperty.APPROX_PROCESS_WHOLE_MULTIBUCKET);
    }

    public boolean isApproxCheckKeyInterval() {
        return configuration.getBoolProperty(MIndexProperty.APPROX_CHECK_KEYINTERVAL);
    }
    
    public boolean isMultiBuckets() {
        return configuration.getLongProperty(MIndexProperty.BUCKET_MIN_OCCUPATION) > 0L;
        //return getBucketDispatcher().getBucketLowOccupation() > 0;
    }
        
    public boolean isAgile() {
        return isMultiBuckets() && configuration.getBoolProperty(MIndexProperty.AGILE_SPLIT);
    }
    
    public boolean isPreciseSearch() {
        return ppCalculator.isPreciseSearch();
    }
    
    public int getBucketCapacity() {
        return (int) configuration.getLongProperty(MIndexProperty.BUCKET_MAX_CAPACITY);
    }
        
    public boolean isBucketOccupationInBytes() {
        return configuration.getBoolProperty(MIndexProperty.OCCUPATION_IN_BYTES);
    }

//    public long getMaxObjectNumber() {        
//        return configuration.getLongProperty(MIndexProperty.MAX_OBJECT_NUMBER);
//    }
    
    public boolean isCheckDuplicateDC() {
        return ppCalculator.isCheckDuplicateDC();
    }
    
    // *************************************  Setter for configuration properties   ****************** //
    
    /**
     * This setter is used only for experimental reasons.
     * @param usePivotFiltering new value of the "use_pivot_filtering" flag
     */
    public void setUsePivotFiltering(boolean usePivotFiltering) {
        configuration.setProperty(MIndexProperty.PIVOT_FILTERING, String.valueOf(usePivotFiltering));
    }    
    
    
    // **************************************    Initialization   ************************************* //

    /**
     * Creates new instance of M-Index having a file name of M-Index properties.
     * @param origConfiguration created properties specifying M-Index configuration
     * @param prefix prefix of the properties (e.g. "mindex.") for this layer of M-Index
     * @throws java.lang.InstantiationException
     */
    public MetricIndex(Properties origConfiguration, String prefix) throws InstantiationException, AlgorithmMethodException {
        this(new MIndexProperties(origConfiguration, prefix));
        try {
            // initialize configuration for overfilled buckets (split is not possible any more)
            this.overfilledBucketClass =  configuration.getClassProperty(MIndexProperty.OVERFILLED_BUCKET_CLASS.getKey(), false, LocalBucket.class);
            this.overfilledBucketParams = fillBucketProperties(MIndexProperty.PREFIX_OVERFILLED_BUCKET_PARAMS, configuration);            
        } catch (ExtendedPropertiesException | NumberFormatException ex) {
            MetricIndexes.logger.log(Level.SEVERE, ex.toString(), ex);
            throw new InstantiationException(ex.toString());
        }
    }

    /**
     * Creates new instance of M-Index having practically all components already initialized.
     * @param config properties specifying M-Index configuration
     * @throws java.lang.InstantiationException
     */
    protected MetricIndex(MIndexProperties config) throws InstantiationException, AlgorithmMethodException {
        this.configuration = config;
        this.ppCalculator = initPPCalculator(config);
        this.voronoiCellTree = initVoronoiCellTree();
        this.bucketDispatcher = initBucketDispatcher(config);
    }
    
    protected VoronoiInternalCell initVoronoiCellTree() throws AlgorithmMethodException {
        return isPreciseSearch() ? new MIndexPreciseInternalCell(this, null, new short[0]) 
                : new MIndexInternalCell(this, null, new short[0]);
    }
    
    protected MIndexObjectInitilizer initPPCalculator(MIndexProperties config) throws InstantiationException {
        return new MIndexStdInitializer(config);
    }
    
    protected BucketDispatcher initBucketDispatcher(MIndexProperties config) throws InstantiationException {
        return createBucketDispatcher(config);
    }
    
    public static BucketDispatcher createBucketDispatcher(MIndexProperties configuration) throws InstantiationException {
    	Class<? extends LocalBucket> bucketClass = configuration.getClassProperty(MIndexProperty.BUCKET_CLASS.getKey(), true, LocalBucket.class);
        Map<String, Object> bucketClassParams = fillBucketProperties(MIndexProperty.PREFIX_BUCKET_PARAMS, configuration);
        long bucketHardCapacity = configuration.getLongProperty(MIndexProperty.BUCKET_MAX_CAPACITY);
        boolean bucketOccupationAsBytes = configuration.getBoolProperty(MIndexProperty.OCCUPATION_IN_BYTES);
        //long bucketMinOccupation = configuration.getLongProperty(MIndexProperty.BUCKET_MIN_OCCUPATION);
        return new BucketDispatcher(Integer.MAX_VALUE, bucketHardCapacity, bucketHardCapacity, 0L, bucketOccupationAsBytes, bucketClass, bucketClassParams);
    } 

    /**
     * THis method fills and returns properties for bucket creating from this configuration.
     *   String THESE_PROPERTIES is substituted by the original configuration properties file.
     * @param paramsPrefix prefix of the bucket properties, e.g. "bucket.params."
     * @param configuration the whole M-Index configuration  
     * @return new configuration properties for bucket creation
     */
    public static Map<String, Object> fillBucketProperties(String paramsPrefix, Properties configuration) {
        Map<String, Object> retval = new HashMap<>();
        for (String key : configuration.stringPropertyNames()) {
            if (key.startsWith(paramsPrefix)) {
                if ("THESE_PROPERTIES".equals(configuration.getProperty(key))) {
                    retval.put(key.substring(paramsPrefix.length()), configuration);
                } else {
                    retval.put(key.substring(paramsPrefix.length()), configuration.getProperty(key));
                }
            }
        }
        return retval;
    }
    
    @Override
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    public void finalize() throws Throwable {
        if (bucketDispatcher != null) {
            bucketDispatcher.finalize();
        }
        super.finalize();
    }

    public void destroy() throws Throwable {
        if (bucketDispatcher != null) {
            bucketDispatcher.destroy();
        }
    }

    @Override
    public ExecutorService setOperationsThreadPool(ExecutorService operationsThreadPool) {
        if (ppCalculator instanceof ThreadPoolUser) {
            return ((ThreadPoolUser) ppCalculator).setOperationsThreadPool(operationsThreadPool);
        }
        return null;
    }

    // *********************  Methods taking care about the storage and dynamic levels of M-Index  ********************** //
        
    /**
     * Return the number of objects stored by this M-Index.
     * @return the number of objects stored by this M-Index.
     */
    public int getObjectCount() {
        return (bucketDispatcher != null) ? bucketDispatcher.getObjectCount() : voronoiCellTree.getObjectCount();
    }

    /**
     * The given bucket is removed from management of this M-Index (from respective bucket dispatcher).
     * @param bucket bucket to be removed
     * @param ignoreNonexisting 
     * @throws AlgorithmMethodException is this bucket is not managed by this M-Index
     */
    public void removeBucket(Bucket bucket, boolean ignoreNonexisting) throws AlgorithmMethodException {
        try {
            bucketDispatcher.removeBucket(bucket.getBucketID());
        } catch (NoSuchElementException e) {
            if (! ignoreNonexisting) {
                throw new AlgorithmMethodException("Given bucket is not managed by this M-Index and cannot be removed: " + e.getLocalizedMessage());
            }
        }
    }

    /**
     * Creates a bucket for given leaf cell.
     */
    public Bucket createBucket(MIndexLeafCell leaf) throws BucketStorageException, IOException, AlgorithmMethodException {
        return bucketDispatcher.createBucket();
    }

    /**
     * Sets the period of the automatic temporary-closeable checking of the buckets.
     * @param period the new checking period in milliseconds;
     *          zero value means disable the checking
     */
    public void autoCloseBuckets(long period) {
        bucketDispatcher.setTemporaryClosePeriod(period);        
    }
    
    /**
     * This method is overridden in the DistributedMIndex
     * @param bucket
     * @param policy
     * @param whoStays
     * @return
     * @throws BucketStorageException 
     */
    public SplitResult splitLeafBucket(Bucket bucket, final SplitPolicy policy, int whoStays) throws BucketStorageException {
        final List<Bucket> newBuckets = new ArrayList<>(getNumberOfPivots());
        final int objectsMoved = bucket.split(policy, newBuckets, bucketDispatcher, whoStays);
        
        return new SplitResult() {
            @Override
            public List<? extends Bucket> getBuckets(BucketDispatcher bucketDisp) throws CapacityFullException {
                return newBuckets;
            }
            @Override
            public SplitPolicy getSplitPolicy() {
                return policy;
            }
            @Override
            public int getObjectsMoved() {
                return objectsMoved;
            }
        };
    }
    
    /**
     * If a special bucket for overfilled cells is specified than the data is reorganized in this new bucket.
     *  If the data cannot be reorganized (the special bucket type is not set or it has already been reorganized)
     *  then NULL is returned.
     * 
     * @param origBucket bucket to be reorganized 
     * @return new created bucket or <code>null</code>, if the data cannot be reorganized 
     *      (the special bucket type is not set or it has already been reorganized)
     * @throws AlgorithmMethodException if anything goes wrong during new bucket creation
     */
    public Bucket reorganizeOvefilled(Bucket origBucket) throws AlgorithmMethodException {
        try {
            LocalBucket newBucket;
            if (! isSpecialOverfilledBuckets() || overfilledBucketClass.isInstance(origBucket)) {
                newBucket = bucketDispatcher.createBucket(Long.MAX_VALUE, Long.MAX_VALUE, bucketDispatcher.getBucketLowOccupation());
            } else {
                newBucket = bucketDispatcher.createBucket(overfilledBucketClass, overfilledBucketParams, Long.MAX_VALUE, Long.MAX_VALUE, bucketDispatcher.getBucketLowOccupation());
            }
            try {
                newBucket.addObjects(origBucket.getAllObjects());
            } catch (NoSuchElementException e) {
                MetricIndexes.logger.warning("reorganization of overfilled bucket initialized but the bucket was actually empty: ID=" + origBucket.getBucketID());
            }
            try {
                bucketDispatcher.removeBucket(origBucket.getBucketID());
            } catch (NoSuchElementException e) {
                MetricIndexes.logger.warning("error deleting original bucket during reorganization: ID=" + origBucket.getBucketID());
            }

            return newBucket;
        } catch (BucketStorageException ex) {
            MetricIndexes.logger.log(Level.SEVERE, ex.toString(), ex);
            throw new AlgorithmMethodException(ex);
        }
    }

    /** Set the new value of the flag {@link MetricIndex#dynamicTreeModified} */
    public void setDynamicTreeModified(boolean dynamicTreeModified) {
        this.dynamicTreeModified = dynamicTreeModified;
    }

    /** Returns the actual value of {@link MetricIndex#dynamicTreeModified} */
    public boolean isDynamicTreeModified() {
        return dynamicTreeModified;
    }
    
    /** 
     * Clears (removes) all the data stored in this M-Index. The structure of the 
     *  dynamic cell tree remains untouched.
     * @throws messif.algorithms.AlgorithmMethodException
     */
    public void clearData() throws AlgorithmMethodException {
        voronoiCellTree.clearData();
    }
    
    /**
     * THis method initializes the object's PPP, then traverses the dynamic PPP-Tree and returns 
     *  the dynamic hash number for given PPP and actual level.
     * @param object data object to find
     * @return the dynamic M-Index key (long) for given object (PP) and level of that specific PP in current PPP-Tree
     * @throws AlgorithmMethodException 
     */
    public long getDynamicKey(LocalAbstractObject object) throws AlgorithmMethodException {
        short[] objectPPP = ppCalculator.getObjectPPP(object);
        VoronoiLeafCell responsibleLeaf = voronoiCellTree.getResponsibleLeaf(objectPPP, false);
        return ppCalculator.computeClusterNumber(objectPPP, responsibleLeaf.getLevel());
    }

    public long [] getListOfLeaves() {
        return null;
    }
    
    // ************************  Key calculation and other key manipulation methods  ********************** //

    /**
     * Initialize the object in M-Index: calculate all distances, sort them, set pivot permutation (and set
     *   filter, if the filter should be stored by this M-Index).
     * @param object object to initialize
     * @return the created M-Index Key (in the form of distance filter, at the moment
     * @throws messif.algorithms.AlgorithmMethodException)
     */
    public LocalAbstractObject initPP(LocalAbstractObject object) throws AlgorithmMethodException {
        return ppCalculator.initPP(object);
    }

    /**
     * Initialize the object in M-Index: calculate all distances, sort them, set pivot permutation (and set
     *   filter, if the filter should be stored by this M-Index).
     * @param object object to initialize
     * @return distances to pivots
     */
    public float [] initPPGetDistances(LocalAbstractObject object) throws AlgorithmMethodException {
        return ppCalculator.initPPGetDistances(object);
    }
    
    /**
     * Initialize the objects in M-Index: calculate all distances, sort them, set pivot permutation (and set
     *   filter, if the filter should be stored by this M-Index).
     * @param objects objects to initialize
     * @return the collection containing the same objects but initialized (they might be changed, e.g. for PPPObjects)
     * @throws messif.algorithms.AlgorithmMethodException
     */
    public Collection<? extends LocalAbstractObject> initPP(Collection<? extends LocalAbstractObject> objects) throws AlgorithmMethodException {
    	return ppCalculator.initPP(objects);
    }


    /** TODO: this method should be in future on "AbstractObject"
     * Given an object, this method simply returns its {@link MIndexObjectPivotPermutation}, if it exists, 
     *  null is returned otherwise.
     * @param object data object to read pivot permutation from
     * @return {@link MIndexObjectPivotPermutation}, if it exists, null is returned otherwise
     */
    public short [] readPPP(LocalAbstractObject object) {
        MIndexPP objectPP = object.getDistanceFilter(MIndexPPSimple.class);
        if (objectPP == null) {
            objectPP = object.getDistanceFilter(MIndexKeyPPFull.class);
        }
        return objectPP.getPPPForReading();
    }
    
        
    // *******************************   Methods processing operations *************************************** //
    
    /** Pre-created list of supported operations. */
    private final static List<Class<? extends AbstractOperation>> supportedOperations = 
            Collections.unmodifiableList(Arrays.asList(ApproxKNNQueryOperation.class, ApproxRangeQueryOperation.class,
            BulkInsertOperation.class, InsertOperation.class, DeleteOperation.class,
            RangeQueryOperation.class, KNNQueryOperation.class, GetRandomObjectsQueryOperation.class));
    
    /**
     * Returns an unmodifiable list of ALL operation classes supported by M-Index.
     * @return the list of ALL operation classes supported by M-Index.
     */
    public List<Class<? extends AbstractOperation>> getSupportedOpList() {
        return MetricIndex.supportedOperations;
    }
    
    /**
     * Method that creates specific implementation of NavigationProcessor for
     * each type of operation
     * @param operation to be processed
     * @return a specific implementation of NavigationProcessor or null if
     */
    @Override
    public NavigationProcessor<? extends AbstractOperation> getNavigationProcessor(AbstractOperation operation) {
        try {
            if (operation instanceof ApproxKNNQueryOperation) {
                return new ApproxNavigationProcessor((ApproxKNNQueryOperation) operation, (ApproxKNNQueryOperation) operation, this);
            }
            if (operation instanceof ApproxRangeQueryOperation) {
                return new ApproxNavigationProcessor((ApproxRangeQueryOperation) operation, (ApproxRangeQueryOperation) operation, this);
            }
            if (operation instanceof KNNQueryOperation) {
                return new KNNProgressiveNavigationProcessor((KNNQueryOperation) operation, this);
            }
            if (operation instanceof RangeQueryOperation && isPreciseSearch()) {
                return new RangeNavigationProcessor((RangeQueryOperation) operation, this);
            }                

            // get random objects
            if (operation instanceof GetRandomObjectsQueryOperation) {
                return new GetRandomObjectsNavigationProcessor((GetRandomObjectsQueryOperation) operation, voronoiCellTree);
            }
            
            // For InsertOperation, BulkInsertOperation and DeleteOperation
            if (operation instanceof DataManipulationOperation) {
                return DataManipulationNavigationProcessor.getInstance(operation, this);
            }
        } catch (AlgorithmMethodException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    
    // *********************************************   Auxilionary methods   ********************************* //

    /**
     * Prints detailed information about this M-Index - including the dynamic cluster tree
     *  and numbers of objects stored in individual clusters.
     * WARNING: opens all disk buckets (files)
     * @return string representation of this M-Index
     */
    public String toRichString() {
        StringBuilder strBuf = new StringBuilder(toString());

        // print the dynamic levels including number of objects stored
        strBuf.append(voronoiCellTree.toString());
        if (bucketDispatcher != null) {
            strBuf.append("\nPhysical buckets:\n");
            int sumOfObjects = 0;
            for (LocalBucket bucket : bucketDispatcher.getAllBuckets()) {
                strBuf.append("\tbucket ").append(bucket.getBucketID()).append(": storing ").append(bucket.getOccupation()).append("\n");
                sumOfObjects += bucket.getOccupation();
            }
            strBuf.append("\nThe bucket dispatcher stores: ").append(sumOfObjects).append(" objects (bytes)\n");
        }
        return strBuf.toString();
    }

    /** 
     * Prints info about this M-Index.
     * @return string representation of this M-Index
     */
    @Override
    public String toString() {
        StringBuilder strBuf = new StringBuilder("M-Index: \n");
        for (String property : configuration.stringPropertyNames()) {
            strBuf.append("\t").append(property).append(" = ").append(configuration.getProperty(property)).append('\n');
        }
        // pivot settings information
        strBuf.append(ppCalculator.toString());
        
        // storage information
        strBuf.append("\nStorage: ").append(bucketDispatcher).append("\n");      
        strBuf.append("use the ").append(isAgile() ? "AGILE" : "LAZY").append(" approach for partitioning\n");
        strBuf.append("\nStoring: ").append(voronoiCellTree.getObjectCount()).append(" objects\n");
        return strBuf.toString();
    }
}

/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import mindex.MetricIndex;

/**
 * This class calculates distance between a query object represented by query-pivot distances
 *  and a data object PPP using the "sum of query-pivot distance differences" normalized by
 *  mutual distances between the pivots. This calculator was used in the previous versions of M-Index.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class QPDistDiffCalculatorDistNorm extends QueryPivotDistDiffCalculator {

    /** M-Index configuration and method provider */
    protected final MetricIndex mIndex;
    
    /**
     * Creates the calculator from given query object and the M-Index configuration
     * @param queryObject query object
     * @param mIndex M-Index configuration
     */
    public QPDistDiffCalculatorDistNorm(LocalAbstractObject queryObject, MetricIndex mIndex) throws AlgorithmMethodException {
        super(queryObject, mIndex);
        this.mIndex = mIndex;
    }

    
    // ************** Query-PPP distance calculation ***************//

    @Override
    public float getQueryDistance(short[] ppp) {        
        float sum = 0f;
        for (int i = 0; i < getMaxLevel(ppp.length); i++) {
            float add = Math.max(queryPivotDistances[ppp[i]] - queryPivotDistances[queryPP[i]], 0f);
            
            float pivotsDistance = mIndex.getPivotsDistance(ppp[i], queryPP[i]);

            sum += (pivotsDistance <= 0f) ? add : add / pivotsDistance;
        }
        return sum;
    }
    

}

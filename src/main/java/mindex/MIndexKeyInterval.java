/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.Serializable;
import messif.objects.keys.KeyInterval;

/**
 * This is a simple encapsulation of interval of closed interval of M-Index keys.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexKeyInterval extends KeyInterval<MIndexKey> implements Serializable {
    
    /** Class serial ID for serialization. */
    private static final long serialVersionUID = 62302L;
    
    /**
     * Lower bound (inclusive).
     */
    protected MIndexKey from;
    
    /**
     * Upper bound (inclusive).
     */
    protected MIndexKey to;
    
    /**
     * Level of M-Index on which these intervals are
     */
    
    /**
     * Returns the lower bound.
     * @return the lower bound.
     */
    @Override
    public final MIndexKey getFrom() {
        return from;
    }

    /**
     * Returns the upper bound.
     * @return the upper bound.
     */
    @Override
    public final MIndexKey getTo() {
        return to;
    }

    /**
     * Set the lower key.
     */
    public void setFrom(MIndexKey from) {
        this.from = from;
    }

    /**
     * Set the upper key
     */
    public void setTo(MIndexKey to) {
        this.to = to;
    }    
    
    /**
     * Constructor for this interval.
     * @param from lower bound (inclusive)
     * @param to upper bound (inclusive)
     */
    public MIndexKeyInterval(MIndexKey from, MIndexKey to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public int compareTo(KeyInterval<MIndexKey> o) {
        return from.compareTo(o.getFrom());
    }
}

/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.algorithms;

import messif.algorithms.AlgorithmMethodException;
import messif.operations.AbstractOperation;
import mindex.MetricIndex;

/**
 * This class encapsulates possible auxiliary operations to be processed on centralized M-Index.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public abstract class AuxiliaryOperation extends AbstractOperation {

    /**
     * This method processes the operation on a given (centralized) M-Index.
     * @param mIndex M-Index to process the auxiliary operation on
     * @throws AlgorithmMethodException if the processing goes wrong
     */
    public abstract void process(MetricIndex mIndex) throws AlgorithmMethodException;

}

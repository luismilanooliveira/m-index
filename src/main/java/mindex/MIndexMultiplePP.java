/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import messif.algorithms.Algorithm;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFilter;
import messif.operations.AbstractOperation;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.InsertOperation;

     
/**
 * Class encapsulating a list of M-Index PPs used for initialization of multiple-overlay M-Index.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
@Deprecated
public class MIndexMultiplePP extends PrecomputedDistancesFilter {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 723101L;

    /** The list of PP for the multiple M-Indexes */
    private final List<PrecomputedDistancesFilter> pivotPermutations;

    /**
     * Initialize the instance for an expected number of overlays.
     * @param nOverlays 
     */
    public MIndexMultiplePP(int nOverlays) {
        this.pivotPermutations = new ArrayList<>(nOverlays);
    }

    public void addMIndexPP(PrecomputedDistancesFilter pp) {
        pivotPermutations.add(pp);
    }
    
    public Iterator<PrecomputedDistancesFilter> getPivotPermuations() {
        return pivotPermutations.iterator();
    }
    
    public PrecomputedDistancesFilter getPivotPermutation(int index) {
        return pivotPermutations.get(index);
    }    

    // code previously used in MultipleMIndexAlgorithm.getNavigationProcessor
    
//
//                /**
//                 * This method makes sense only if the processed objects (inserted/deleted) have the M-Index keys set within
//                 *  the MIndexMultiplePP. Otherwise, the method does not do anything.
//                 */
//                private void setRelevantMIndexPPs(Algorithm algorithm, AbstractOperation operation) throws IllegalArgumentException, NullPointerException {
//                    // check and modify the M-Index keys, if already preset
//                    int algIndex = Arrays.asList(algorithms).indexOf(algorithm);
//                    if (operation instanceof InsertOperation) {
//                        LocalAbstractObject insertedObject = ((InsertOperation) operation).getInsertedObject();
//                        setGivenPP(insertedObject, algIndex);
//                    } else if (operation instanceof BulkInsertOperation) {
//                        for (LocalAbstractObject obj : ((BulkInsertOperation) operation).getInsertedObjects()) {
//                            if (!setGivenPP(obj, algIndex)) {
//                                break;
//                            }
//                        }
//                    }
//                }
//
//                /**
//                 * This method makes sense only if the processed objects (inserted/deleted) have the M-Index keys set within
//                 *  the MIndexMultiplePP. Otherwise, the method does not do anything.
//                 */
//                private boolean setGivenPP(LocalAbstractObject insertedObject, int algIndex) throws IllegalArgumentException, NullPointerException {
//                    MIndexMultiplePP multiplePP = insertedObject.getDistanceFilter(MIndexMultiplePP.class);
//                    if (multiplePP == null) {
//                        return false;
//                    }
//                    insertedObject.unchainFilter(multiplePP);
//                    insertedObject.chainFilter(multiplePP.getPivotPermutation(algIndex), true);
//                    return true;
//                }
//            };
    
    
    
    // TODO: REMOVE WHEN IT WON'T BE FILTER ANY MORE
    
    @Override
    public float getPrecomputedDistance(LocalAbstractObject obj, float[] metaDistances) {
        return LocalAbstractObject.UNKNOWN_DISTANCE;
    }

    @Override
    public boolean excludeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        return false;
    }

    @Override
    public boolean includeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        return false;
    }

    @Override
    protected boolean addPrecomputedDistance(LocalAbstractObject obj, float distance, float[] metaDistances) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected boolean isDataWritable() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

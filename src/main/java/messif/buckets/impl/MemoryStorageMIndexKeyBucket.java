/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.buckets.impl;


import messif.objects.LocalAbstractObject;
import java.io.Serializable;
import messif.buckets.BucketDispatcher;
import messif.buckets.LocalBucket;
import messif.buckets.OrderedLocalBucket;
import messif.buckets.index.IndexComparator;
import messif.buckets.index.ModifiableOrderedIndex;
import messif.buckets.index.impl.IntStorageIndex;
import messif.buckets.storage.impl.MemoryStorage;
import mindex.MIndexKey;
import mindex.MIndexPP;
import mindex.MetricIndexes;


/**
 * A volatile implementation of {@link LocalBucket}.
 * It stores all objects in a {@link messif.buckets.storage.impl.MemoryStorage memory storage}.
 * Objects are indexed by their {@link LocalAbstractObject#getObjectKey() object keys} and
 * iterator will return the objects ordered.
 * 
 * <p>
 * This bucket has an efficient {@link LocalBucket#getObject(java.lang.String)} implementation
 * at the cost of additional memory overhead for maintaining the index.
 * If fast {@code getObject} implementation is not required and
 * the iteration over all objects is used frequently, consider using
 * {@link MemoryStorageBucket}.
 * </p>
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 * @see BucketDispatcher
 * @see LocalBucket
 */
public class MemoryStorageMIndexKeyBucket extends OrderedLocalBucket<MIndexKey> implements Serializable {
    /** class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Data storage ******************//
    /** Object storage with object-id index */
    protected final ModifiableOrderedIndex<MIndexKey, LocalAbstractObject> objects;


    /****************** Constructors ******************/

    /**
     * Constructs a new instance of MemoryStorageObjectKeyBucket.
     * 
     * @param capacity maximal capacity of the bucket - cannot be exceeded
     * @param softCapacity maximal soft capacity of the bucket
     * @param lowOccupation a minimal occupation for deleting objects - cannot be lowered
     * @param occupationAsBytes flag whether the occupation (and thus all the limits) are in bytes or number of objects
     */
    public MemoryStorageMIndexKeyBucket(short cellLevel, long capacity, long softCapacity, long lowOccupation, boolean occupationAsBytes) {
        super(capacity, softCapacity, lowOccupation, occupationAsBytes, 0L);
        this.objects = new IntStorageIndex<>(new MemoryStorage<>(LocalAbstractObject.class), new MIndexKeyToLocalObjectComparator(cellLevel));
    }



    //****************** Overrides ******************//

    @Override
    protected ModifiableOrderedIndex<MIndexKey, LocalAbstractObject> getModifiableIndex() {
        return objects;
    }

    @Override
    public void finalize() throws Throwable {
        objects.finalize();
        super.finalize();
    }

    @Override
    public void destroy() throws Throwable {
        objects.destroy();
        super.destroy();
    }

    
    // *********************************   Comparator  ************************************ //
        public static IndexComparator<MIndexKey, LocalAbstractObject> createMIndexKeyComparator(short cellLevel) {
        return new MIndexKeyToLocalObjectComparator(cellLevel);
    }
    
    /** Index order defined by object keys */
//    public static IndexComparator<MIndexKey, LocalAbstractObject> mIndexKeyToLocalObjectComparator = 
    protected static class MIndexKeyToLocalObjectComparator implements IndexComparator<MIndexKey, LocalAbstractObject> {
        /** Class serial id for serialization. */
        private static final long serialVersionUID = 25202L;

        /** M-Index cell level (in order to calculate the M-Index key right) */
        short level;

        public MIndexKeyToLocalObjectComparator(short level) {
            this.level = level;
        }
        
        @Override
        public int indexCompare(MIndexKey o1, LocalAbstractObject o2) {
            return compare(o1, extractKey(o2));
        }

        @Override
        public int compare(MIndexKey o1, MIndexKey o2) {
            return o1.compareTo(o2);
        }

        @Override
        public MIndexKey extractKey(LocalAbstractObject object) {
            MIndexPP mIndexPP = MetricIndexes.readMIndexPP(object);
            mIndexPP.getClusterNumber(level);
            return (MIndexKey) mIndexPP;
        }

        @Override
        public boolean equals(Object obj) {
            return obj.getClass() == this.getClass();
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    };
    

}

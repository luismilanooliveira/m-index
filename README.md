# Welcome to M-Index project
The M-Index is an indexing structure for similarity search in data modeled as generic metric spaces. The M-Index can organize your data collection based on a _similarity function_ (distance) and then quickly evaluate _similarity queries_ like _k-nearest neighbors query_ (give me _k_ indexed objects most similar to this example) or _range queries_ (give me all indexed objects in distance smaller than given radius).

The structure was developed and this project initiazed by the [DISA laboratory](http://disa.fi.muni.cz/) at Masaryk University, Brno, Czech Republic. This implementation is used in most of [demo application](http://disa.fi.muni.cz/prototype-applications/) of DISA. The package is a Java Maven project containing both centralized M-Index and its distributed version (restricted functionality).

Details on the M-Index structure are described in these papers:

* David Novak, Michal Batko, Pavel Zezula. (2011). Metric Index: An Efficient and Scalable Solution for Precise and Approximate Similarity Search. Information Systems, 36(4), 721–733. [DOI:10.1016/j.is.2010.10.002](http://dx.doi.org/10.1016/j.is.2010.10.002)

* David Novak, Michal Batko, Pavel Zezula. (2012). Large-scale similarity data management with distributed Metric Index. Information Processing & Management, 48(5), 855–872. [DOI:10.1016/j.ipm.2010.12.004](http://dx.doi.org/10.1016/j.ipm.2010.12.004)

* David Novak, Michal Batko. Metric Index: An Efficient and Scalable Solution for Similarity Search. In ”Proceedings of SISAP 2009”. IEEE Computer Society. 2009. [DOI:10.1109/SISAP.2009.26](http://dx.doi.org/10.1109/SISAP.2009.26)

If this code helps you in your research, please, refer these publications (the first one).

## Contact Person
David Novak, [homepage](http://disa.fi.muni.cz/david-novak/)

## Licence
The owner of the result is Masaryk University, a public high school, ID: 00216224. Masaryk University allows other companies and individuals to use this software free of charge and without territorial restrictions under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. M-Index library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with M-Index library. If not, see [http://www.gnu.org/licenses/].